import { trigger } from '@angular/animations';
import { Component, OnDestroy } from '@angular/core';

import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';

import { Observable, Subject, Observer, Subscription } from 'rxjs';
import { ImageService } from './services/image.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnDestroy {
  public videoOptions: MediaTrackConstraints = {
    width: 1024,
    height: 576,
  };
  public errors: WebcamInitError[] = [];

  // latest snapshot

  public webcamImage: WebcamImage = null;
  generatedImage: string;
  imageFile: File;
  imageHash: string;
  sub: Subscription;
  pleaseWait = false;
  isPreviewVisible = true;
  togglePreviewMessage = 'prévisualisation activées';
  pictureProcessed = false;
  message: string;


  constructor(private imageService: ImageService) {

  }
  // webcam snpashot trigger

  private trigger: Subject<void> = new Subject<void>();

  public triggerSnapshot(): void {
    this.trigger.next();
  }
  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }
  public handleImage(webcamImage: WebcamImage): void {
    this.pictureProcessed = false;
    console.log('received webcam image ', webcamImage);
    this.webcamImage = webcamImage;
    this.dataURItoBlob(webcamImage.imageAsBase64).subscribe(
      (blob) => {
        const imageBlob: Blob = blob;
        const imageName: string = `${new Date().toISOString()}.jpg`;
        this.imageFile = new File([imageBlob], imageName, { type: 'image/jpg' });
      },
    );
  }
  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }
  // Method to convert Base64Data Url as Image Blob
  // https://medium.com/better-programming/convert-a-base64-url-to-image-file-in-angular-4-5796a19fdc21
  dataURItoBlob(dataURI: string): Observable<Blob> {
    return Observable.create((observer: Observer<Blob>) => {
      const byteString: string = window.atob(dataURI);
      const arrayBuffer: ArrayBuffer = new ArrayBuffer(byteString.length);
      const int8Array: Uint8Array = new Uint8Array(arrayBuffer);
      for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
      }
      const blob = new Blob([int8Array], { type: 'image/jpeg' });
      observer.next(blob);
      observer.complete();
    });
  }
  upload() {
    this.pleaseWait = true;
    const formData = new FormData();
    formData.append('files', this.imageFile, this.imageFile.name);
    this.sub = this.imageService.uploadImage(formData).subscribe(data => {
      console.log('appComponent | upload | data', data);
      this.pleaseWait = false;
      this.imageHash = data[0].hash;
      this.message = 'image sauvegardée';
      this.pictureProcessed = true;
      this.showMessage({ duration: 2000 });
    }, err => {
      console.error('appComponent | upload | err', err);
      this.pleaseWait = false;
    });
  }
  togglePreviewVisibility() {
    this.isPreviewVisible = !this.isPreviewVisible;
    this.togglePreviewMessage = this.isPreviewVisible ? 'previsualisation activée' : 'previsualisation desactivée';
  }
  showMessage(option) {
    setTimeout(() => {
      this.message = ''
    }, option.duration);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}

