import { Member } from './../models/member';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.scss']
})
export class MemberDetailsComponent implements OnInit {
  @Input() member: Member;
  url = 'http://localhost:1337/uploads/'

  constructor() { }

  ngOnInit(): void {
  }

}
