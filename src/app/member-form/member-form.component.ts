import { Member } from './../models/member';
import { MemberService } from './../services/member.service';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IDatePickerConfig } from 'ng2-date-picker';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-member-form',
  templateUrl: './member-form.component.html',
  styleUrls: ['./member-form.component.scss']
})
export class MemberFormComponent implements OnInit, OnDestroy {
  @Input() imageName;
  showMemberDetails = false;
  saveMember: Member;
  createMember: FormGroup;
  datePickerConfig: IDatePickerConfig = { locale: 'fr', showMultipleYearsNavigation: true };
  sub: Subscription;

  constructor(private fb: FormBuilder, private memberService: MemberService) { }

  ngOnInit(): void {
    this.createMember = this.fb.group({
      name: ['', Validators.required],
      firstName: ['', Validators.required],
      dob: [null],
      dateOfSubscription: [null],
      imageName: this.imageName,
      address: this.fb.group({
        street: [''],
        streetComplement: [''],
        zip: [''],
        town: [''],
      })
    });
  }
  create() {
    let member: Member = this.createMember.value
    console.log('member', member);
    member = { ...this.createMember.value, dob: new Date(member.dob), dateOfSubscription: new Date(member.dateOfSubscription), }
    this.sub = this.memberService.create(member).subscribe((saveMember: Member) => {
      console.log('saveMember', saveMember);
      this.showMemberDetails = true;
      this.saveMember = saveMember;
    }, (err) => {
      console.error(err);
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
